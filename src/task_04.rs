use super::*;

#[derive(Debug, PartialEq)]
enum Length {
    Metric(u32),
    US(u32),
    Invalid,
}

#[derive(Debug, PartialEq, Default)]
struct Passport {
    required_present: bool,
    valid: bool,
    byr: Option<u32>,
    iyr: Option<u32>,
    eyr: Option<u32>,
    hcl: Option<String>,
    ecl: Option<String>,
    pid: Option<String>,
    cid: Option<String>,
    height: Option<Length>,
}

fn height(lex: &mut Lexer<Token>) -> Result<Length> {
    let slice = lex.slice();
    let s = slice[..slice.len()].replace("hgt:", "");

    if !s.contains("cm") && !s.contains("in") {
        Ok(Length::Invalid)
    } else if s.contains("cm") {
        s.replace("cm", "")
            .parse()
            .map(Length::Metric)
            .map_err(|e| e.into())
    } else {
        s.replace("in", "")
            .parse()
            .map(Length::US)
            .map_err(|e| e.into())
    }
}

#[derive(Debug, Logos, PartialEq)]
enum Token {
    #[regex(r"[ \n\t\f]+", logos::skip)]
    #[error]
    Error,

    #[regex("byr:[0-9]+", |lex| lex.slice().replace("byr:", "").parse())]
    Byr(u32),

    #[regex("iyr:[0-9]+", |lex| lex.slice().replace("iyr:", "").parse())]
    Iyr(u32),

    #[regex("eyr:[0-9]+", |lex| lex.slice().replace("eyr:", "").parse())]
    Eyr(u32),

    #[regex("pid:#?[a-z0-9]+", |lex| lex.slice().replace("pid:", ""))]
    Pid(String),

    #[regex("cid:#?[a-z0-9]+", |lex| lex.slice().replace("cid:", ""))]
    Cid(String),

    #[regex("hcl:#?[a-z0-9]+", |lex| lex.slice().replace("hcl:", ""))]
    Hcl(String),

    #[regex("ecl:#?[a-z0-9]+", |lex| lex.slice().replace("ecl:", ""))]
    Ecl(String),

    #[regex("hgt:[0-9]+[ci]?[mn]?", height)]
    Hgt(Length),
}

pub(crate) fn task_04_a() -> Result<usize> {
    let contents = fs::read_to_string("data/04.txt")?;

    let res = input_to_passports(&contents)
        .iter()
        .filter(|&x| x.required_present)
        .count();

    Ok(res)
}

pub(crate) fn task_04_b() -> Result<usize> {
    let contents = fs::read_to_string("data/04.txt")?;

    let res = input_to_passports(&contents)
        .iter()
        .filter(|&x| x.valid)
        .count();

    Ok(res)
}

fn input_to_passports(inp: &str) -> Vec<Passport> {
    inp.split("\n\n")
        .map(|line| {
            let line = line.replace("\n", " ").replace("  ", " ");
            // println!("{:?}", line);
            Token::lexer(&line).fold(Passport::default(), |mut acc, x| {
                // println!("{:?}", x);
                match x {
                    Token::Byr(val) => acc.byr = Some(val),
                    Token::Eyr(val) => acc.eyr = Some(val),
                    Token::Iyr(val) => acc.iyr = Some(val),
                    Token::Pid(val) => acc.pid = Some(val),
                    Token::Cid(val) => acc.cid = Some(val),
                    Token::Hcl(val) => acc.hcl = Some(val),
                    Token::Ecl(val) => acc.ecl = Some(val),
                    Token::Hgt(val) => acc.height = Some(val),
                    Token::Error => unreachable!(),
                }
                acc.required_present = acc.byr.is_some()
                    && acc.iyr.is_some()
                    && acc.eyr.is_some()
                    && acc.hcl.is_some()
                    && acc.ecl.is_some()
                    && acc.pid.is_some()
                    && acc.height.is_some();

                acc.valid = acc.required_present
                    && acc.byr >= Some(1920)
                    && acc.byr <= Some(2002)
                    && acc.iyr >= Some(2010)
                    && acc.iyr <= Some(2020)
                    && acc.eyr >= Some(2020)
                    && acc.eyr <= Some(2030)
                    && match acc.height {
                        Some(Length::Invalid) => false,
                        Some(Length::Metric(val)) => val >= 150 && val <= 193,
                        Some(Length::US(val)) => val >= 59 && val <= 76,
                        _ => false,
                    }
                    && if let Some(val) = acc.hcl.clone() {
                        let mut it = val.chars();
                        it.next() == Some('#') && it.filter(|&x| x.is_digit(16)).count() == 6
                    } else {
                        false
                    }
                    && if let Some(val) = acc.ecl.clone() {
                        let v: Vec<String> = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]
                            .iter()
                            .map(|&s| s.to_string())
                            .collect();
                        v.contains(&val)
                    } else {
                        false
                    }
                    && if let Some(val) = acc.pid.clone() {
                        val.chars().filter(|x| x.is_digit(10)).count() == 9
                    } else {
                        false
                    };
                acc
            })
        })
        .collect()
}

#[test]
fn required_present_2_of_4() -> Result<()> {
    let contents = "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
    byr:1937 iyr:2017 cid:147 hgt:183cm

    iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
    hcl:#cfa07d byr:1929

    hcl:#ae17e1 iyr:2013
    eyr:2024
    ecl:brn pid:760753108 byr:1931
    hgt:179cm

    hcl:#cfa07d eyr:2025 pid:166559648
    iyr:2011 ecl:brn hgt:59in";

    let passports = input_to_passports(contents);

    assert_eq!(2, passports.iter().filter(|&x| x.required_present).count());

    Ok(())
}
