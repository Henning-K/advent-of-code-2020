use super::*;

pub(crate) fn task_02_a() -> Result<usize> {
    let contents = fs::read_to_string("data/02.txt")?;
    let res: usize = contents
        .trim()
        .lines()
        .map(|s| s.trim())
        .map(|line| {
            let line: Vec<String> = line.split_whitespace().map(String::from).collect();
            let range: Vec<usize> = line[0].split('-').filter_map(|n| n.parse().ok()).collect();
            let letter = line[1].chars().next().expect("Letter to check is empty.");
            let password = line[2].clone();
            let occurrences = password.chars().filter(|&x| x == letter).count();
            range[0] <= occurrences && occurrences <= range[1]
        })
        .filter(|&x| x)
        .count();

    Ok(res)
}

pub(crate) fn task_02_b() -> Result<usize> {
    let contents = fs::read_to_string("data/02.txt")?;
    let res: usize = contents
        .trim()
        .lines()
        .map(|s| s.trim())
        .map(|line| {
            let line: Vec<String> = line.split_whitespace().map(String::from).collect();
            let range: Vec<usize> = line[0].split('-').filter_map(|n| n.parse().ok()).collect();
            let letter = line[1].chars().next().expect("Letter to check is empty.");
            let password = line[2].clone();
            (password.chars().nth(range[0] - 1).unwrap() == letter)
                ^ (password.chars().nth(range[1] - 1).unwrap() == letter)
        })
        .filter(|&x| x)
        .count();

    Ok(res)
}
