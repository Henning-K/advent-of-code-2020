use super::*;

type Board = Vec<Vec<Square>>;
type Slope = (usize, usize);

#[derive(Debug, PartialEq, Clone)]
enum Square {
    Empty,
    Tree,
}

#[derive(Debug, Copy, Clone, Default)]
struct Toboggan {
    next_x: usize,
    next_y: usize,
    hits: usize,
}

impl Toboggan {
    fn try_slope(&mut self, board: Board, slope: Slope) -> Toboggan {
        while self.next_y < board.len() {
            let line = board[self.next_y].clone();
            self.hits += match line[self.next_x] {
                Square::Empty => 0,
                Square::Tree => 1,
            };
            self.next_x += slope.0;
            self.next_x %= line.len();
            self.next_y += slope.1;
        }
        *self
    }
}

pub(crate) fn task_03_a() -> Result<usize> {
    let contents = fs::read_to_string("data/03.txt")?;
    let board = contents
        .trim()
        .lines()
        .map(|s| s.trim())
        .map(|line| {
            line.chars()
                .map(|c| match c {
                    '.' => Square::Empty,
                    '#' => Square::Tree,
                    _ => unreachable!(),
                })
                .collect::<Vec<_>>()
        })
        .collect();

    let res = Toboggan::default().try_slope(board, (3, 1));

    Ok(res.hits)
}

pub(crate) fn task_03_b() -> Result<usize> {
    let contents = fs::read_to_string("data/03.txt")?;
    let board: Vec<Vec<Square>> = contents
        .trim()
        .lines()
        .map(|s| s.trim())
        .map(|line| {
            line.chars()
                .map(|c| match c {
                    '.' => Square::Empty,
                    '#' => Square::Tree,
                    _ => unreachable!(),
                })
                .collect::<Vec<_>>()
        })
        .collect();

    let res = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
        .iter()
        .map(|&slope| Toboggan::default().try_slope(board.clone(), slope).hits)
        .product();

    Ok(res)
}
