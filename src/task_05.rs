use super::*;

pub(crate) fn task_05_a() -> Result<usize> {
    let contents = fs::read_to_string("data/05.txt")?;

    input_to_seat(&contents)
        .into_iter()
        .max()
        .ok_or_else(|| AOCError::GenericError.into())
}

pub(crate) fn task_05_b() -> Result<usize> {
    let contents = fs::read_to_string("data/05.txt")?;

    let mut seats = input_to_seat(&contents);
    seats.sort();
    let res = seats.windows(2).fold(0, |acc, v| {
        let (a,b) = (v[0], v[1]);
        if a+1 != b {
            a+1
        } else {
            acc
        }
    });

    Ok(res)
}

fn input_to_seat(inp: &str) -> Vec<usize> {
    inp.trim()
        .lines()
        .map(|line| line.trim())
        .map(|line| {
            let (row, col): (Vec<char>, Vec<char>) =
                line.chars().partition(|&c| c == 'F' || c == 'B');

            (row.iter()
                .fold((0usize, 127usize), |acc, &x| match x {
                    'F' => (acc.0, (acc.0 + acc.1) / 2),
                    'B' => ((acc.0 + acc.1 + 1) / 2, acc.1),
                    _ => unreachable!(),
                })
                .0)
                * 8usize
                + (col
                    .iter()
                    .fold((0usize, 7usize), |acc, &x| match x {
                        'L' => (acc.0, (acc.0 + acc.1) / 2),
                        'R' => ((acc.0 + acc.1 + 1) / 2, acc.1),
                        _ => unreachable!(),
                    })
                    .0)
        })
        .collect()
}

#[test]
fn test_boarding_pass_a() -> Result<()> {
    let contents = r#"FBFBBFFRLR
        BFFFBBFRRR
        FFFBBBFRRR
        BBFFBBFRLL"#;

    assert_eq!(vec![357, 567, 119, 820], input_to_seat(contents));

    Ok(())
}
