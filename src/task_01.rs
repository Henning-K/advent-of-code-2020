use super::*;

pub(crate) fn task_01_a() -> Result<i32> {
    let contents = fs::read_to_string("data/01.txt")?;
    let mut nums: Vec<i32> = contents
        .trim()
        .lines()
        .filter_map(|n| n.parse::<i32>().ok())
        .collect::<Vec<i32>>();

    let mut res = -1i32;

    while let Some(fst) = nums.pop() {
        let snd = 2020 - fst;
        if nums.contains(&snd) {
            res = fst * snd;
            break;
        }
    }

    Ok(res)
}

pub(crate) fn task_01_b() -> Result<i32> {
    let contents = fs::read_to_string("data/01.txt")?;
    let mut nums: Vec<i32> = contents
        .trim()
        .lines()
        .filter_map(|n| n.parse::<i32>().ok())
        .collect::<Vec<i32>>();

    let mut res = -1i32;

    'outer: while let Some(fst) = nums.pop() {
        for snd in nums.iter() {
            let trd = 2020 - fst - snd;
            if nums.contains(&trd) {
                res = fst * snd * trd;
                break 'outer;
            }
        }
    }

    Ok(res)
}
